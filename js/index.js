function toggleMenu() {
  var x = document.getElementById("myTopnav");
  if (x.className === "collapse") {
    x.className += " responsive";
  } else {
    x.className = "collapse";
  }
}
$(".variable").slick({
  dots: true,
  infinite: false,
  variableWidth: true,
});
$(function () {
  var $status = $('.count');
  var $slickElement = $('.slider');

  var slideCount = $('.slide-item').length;

  $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    // console.log(slick);
    var i = (currentSlide ? currentSlide : 0) + 1;
    $status.text(i + '/' + slideCount);
  });

  $slickElement.slick({
    dots: true
  });
})